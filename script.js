// content.split(/\s+/);

function word_to_span(word)
{
    var a_span=document.createElement('span');
    var a_text=document.createTextNode(word+' ');
    a_span.appendChild(a_text);
    return a_span;
}


function highlight(the_element, highlight_array)
{
    var the_div=document.getElementById(the_element);
    var the_div_text=the_div.innerHTML;
    var the_div_text_array=the_div_text.split(/\s+/);
    var array_of_spans=[];
    for(var i=0; i<the_div_text_array.length; i++)
    {
        array_of_spans.push(word_to_span(the_div_text_array[i]));
    }
    for(var i=0; i<highlight_array.length; i++)
    {
        array_of_spans[highlight_array[i]].className='keyword';
    }

    document.getElementById(the_element).innerHTML='';

    for(var i=0; i<array_of_spans.length; i++)
    {
        the_div.appendChild(array_of_spans[i]);
    }
}
