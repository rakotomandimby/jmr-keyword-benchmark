<?php
function getRandomWord($l = 3) {
    $word = array_merge(range('a', 'z'));
    shuffle($word);
    return substr(implode($word), 0, rand($l,$l+2));
}

function getRandomContent(){
    $filename='contents/'.sha1(getRandomWord(rand(1,5)).getRandomWord(rand(5,10))).'.txt';
    $content=array();
    $length=rand(10000,30000);
    for($i=0; $i < $length; $i++)
    {
        $content[]=getRandomWord().' ';
    }
    file_put_contents($filename, $content);

}

function getKeywords(){
    $length=10000; // nombres de mot-clés dans le CSV
    for($i=0; $i < $length; $i++)
    {
        $file=fopen('keywords.csv','a');
        fwrite($file , getRandomWord().',');
        fclose($file);
    }
}

function getAWorld(){
    $size=6000; // nombre de contenus à créer
    for($i=0; $i < $size; $i++)
    {
        getRandomContent();
    }
}

// How to use it:

// $ php -a
// > include('generate.php');
// > getKeywords();
// > getAWorld();
// $

// You'll see
// - a file named 'keywords.csv'
// - "contents/" folder full of files

