<?php
//                        keywords.csv
$keywords_handle=fopen('/srv/http/jmr-keyword-benchmark/keywords.csv','r');
$keywords=explode(',',fread($keywords_handle, filesize('/srv/http/jmr-keyword-benchmark/keywords.csv')));
fclose($keywords_handle);

// $keywords is an array();

$contents=scandir('/srv/http/jmr-keyword-benchmark/contents/');
$sub_array=array_slice($contents,2, 100);
$contents=$sub_array;

foreach($contents as $a_content)
{
    $index_filename='indexes/'.explode('.',$a_content)[0].'.js';
    $index_filehandle=fopen($index_filename,'w');
    $content_handle=fopen('contents/'.$a_content,'r');
    $content_words=str_word_count(fread($content_handle, filesize('contents/'.$a_content)),1);
    // on veut obtenir un tableau qui donne les index des keywords, de la forme:
    // [13415,13448,13453,13477,13962,13971,13977,13984,13987,14032,14053,14056,14071,14086,14106,14136,14149,14166]
    fwrite($index_filehandle, '[');
    $tmp=array();
    foreach($content_words as $index=>$word)
    {
        if(in_array($word,$keywords)){$tmp[]=$index;}            
    }
    
    $tmp_length=count($tmp);
    for($i = 0; $i < $tmp_length-1; $i++)
    {  
        fwrite($index_filehandle, $tmp[$i].',');
    }
    fwrite($index_filehandle, $tmp[$tmp_length-1]);
    
    
    fwrite($index_filehandle,']'."\n");        
    fclose($index_filehandle);
    fclose($content_handle);
}
